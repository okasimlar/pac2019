#!/bin/bash

# stop minikube 
minikube stop

# start minikube
minikube start --cpus 4 --memory 8192

# enable ingress
minikube addons enable ingress

# delete all existing
helm delete --purge release-name-conference-ui
helm delete --purge release-name-backend
helm delete --purge neo4j
helm delete --purge grafana
helm delete --purge prometheus
helm delete --purge ingress


# helm install components
helm install --name release-name-conference-ui ../helm/frontend/
helm install --name release-name-backend ../helm/backend/
helm install --name neo4j ../helm/neo4j/
helm install --name grafana ../helm/grafana/
helm install --name prometheus ../helm/prometheus/
helm install --name ingress ../helm/ingress/
kubectl apply -f ../helm/pac2019_cert.yaml



