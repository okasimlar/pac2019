package com.pac.backend.dto;


import com.pac.backend.domain.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Builder
public class TalkDetails {

    @Getter
    @Setter
    private List<Topic> topics;

    @Getter
    @Setter
    private Talk talk;

}
