package com.pac.backend.dto;

import com.pac.backend.domain.Timeslot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Builder
public class RoomSchedule {

    @Getter
    @Setter
    String date;

    @Setter
    @Getter
    List<Timeslot> timeslots;


}
