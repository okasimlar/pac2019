package com.pac.backend.dto;

import com.pac.backend.domain.Room;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Builder
public class RoomDetail {

    @Getter
    @Setter
    private Room room;

    @Getter
    @Setter
    private List<RoomSchedule> roomSchedules;

}
