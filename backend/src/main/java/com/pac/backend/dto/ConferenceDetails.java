package com.pac.backend.dto;


import com.pac.backend.domain.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Builder
public class ConferenceDetails {

    @Getter
    @Setter
    private List<Speaker> speakers;

    @Getter
    @Setter
    private Conference conference;

    @Getter
    @Setter
    private List<Talk> talks;

    @Getter
    @Setter
    private List<Room> rooms;

    @Getter
    @Setter
    private List<Topic> topics;
}
