package com.pac.backend.query;

import com.pac.backend.domain.Timeslot;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeSlotRepository extends Neo4jRepository<Timeslot, Long> {

    @Query("MATCH(t:Talk)-[ts:IS_IN]->(s:Timeslot)-[f:IS_IN]->(r:Room)-[:IS_IN]->(l:Location)" +
            "<-[:TAKES_PLACE]-(c:Conference) " +
            "WHERE (ID(r) = {roomId}) AND (ID(c) = {conferenceId}) return t, ts, s, f, r")
    List<Timeslot> findByRoomIdAndConferenceId(Long roomId, Long conferenceId);

}
