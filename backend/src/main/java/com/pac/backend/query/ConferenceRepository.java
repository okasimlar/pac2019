package com.pac.backend.query;

import com.pac.backend.domain.Conference;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConferenceRepository extends Neo4jRepository<Conference, Long> {

}
