package com.pac.backend.query;

import com.pac.backend.domain.Room;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends Neo4jRepository<Room, Long> {

    @Query("MATCH(r:Room)-[:IS_IN]->(l:Location)<-[:TAKES_PLACE]-(c:Conference) WHERE (ID(c) = {conferenceId}) return (r)")
    List<Room> findByConferenceId(Long conferenceId);

}
