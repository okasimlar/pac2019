package com.pac.backend.query;

import com.pac.backend.domain.Location;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends Neo4jRepository<Location, Long> {

    @Query("MATCH(l:Location)" +
            "WHERE (l.name CONTAINS {searchText}) return l")
    List<Location> findByName(String searchText);
}
