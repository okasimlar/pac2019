package com.pac.backend.query;

import com.pac.backend.domain.Talk;
import com.pac.backend.domain.Topic;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TalkRepository extends Neo4jRepository<Talk, Long> {

    @Query("MATCH(m:Talk)-[:IS_HOLD]->(n:Conference) WHERE (ID(n) = {conferenceId}) return (m)")
    List<Talk> findByConferenceId(Long conferenceId);

    @Query("MATCH(t:Talk)-[ts:IS_IN]->(s:Timeslot)-[:IS_IN]->(r:Room)-[:IS_IN]->(l:Location)<-[:TAKES_PLACE]-(c:Conference) " +
            "WHERE (ID(r) = {roomId}) AND (ID(c) = {conferenceId}) return t, ts, s")
    List<Talk> findByRoomIdAndConferenceId(Long roomId, Long conferenceId);


    @Query("MATCH(t:Topic)<-[h:HAS]-(m:Talk)-[:IS_HOLD]->(n:Conference) WHERE (ID(n) = {conferenceId}) AND (ID(t) " +
            "= {topicId}) return (m)")
    List<Talk> findByTopicIdAndConferenceId(Long topicId, Long conferenceId);

    @Query("MATCH(t:Talk)" +
            "WHERE (t.name CONTAINS {searchText}) return t")
    List<Talk> findByName(String searchText);

}
