package com.pac.backend.query;

import com.pac.backend.domain.Topic;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TopicRepository extends Neo4jRepository<Topic, Long> {

    @Query("MATCH(t:Topic)<-[h:HAS]-(m:Talk)-[:IS_HOLD]->(n:Conference) WHERE (ID(n) = {conferenceId}) return (t)")
    List<Topic> findTopicByConferenceId(Long conferenceId);

    @Query("MATCH(t:Topic)" +
            "WHERE (t.name CONTAINS {searchText}) return t")
    List<Topic> findByName(String searchText);
}
