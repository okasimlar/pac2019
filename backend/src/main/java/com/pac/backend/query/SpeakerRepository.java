package com.pac.backend.query;

import com.pac.backend.domain.Speaker;
import com.pac.backend.domain.Topic;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpeakerRepository extends Neo4jRepository<Speaker, Long> {


    @Query("MATCH(t:Speaker)-[ts:HAS]->(s:Timeslot)-[:IS_IN]->(r:Room)-[:IS_IN]->(l:Location)<-[:TAKES_PLACE]-(c:Conference) " +
            "WHERE (ID(c) = {conferenceId}) return t, ts, s")
    List<Speaker> findSpeakersByConferenceId(Long conferenceId);

    @Query("MATCH(t:Speaker)-[ts:HAS]->(s:Timeslot) " +
            "WHERE (ID(s) = {speakerId}) return t, ts, s")
    Speaker findSpeakerByTimeSlotId(Long speakerId);

    @Query("MATCH(s:Speaker)" +
            "WHERE (s.name CONTAINS {searchText}) return s")
    List<Speaker> findByName(String searchText);
}
