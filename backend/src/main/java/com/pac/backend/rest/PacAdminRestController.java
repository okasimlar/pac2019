package com.pac.backend.rest;

import com.pac.backend.service.ConferenceService;
import com.pac.backend.service.PacAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/admin")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PacAdminRestController {

    @Autowired
    PacAdminService pacAdminService;

    @Autowired
    ConferenceService conferenceService;

    @PutMapping(value = "/import-data")
    @ResponseStatus(HttpStatus.OK)
    //@Secured("admin")
    public void importData(Principal principal) {
        pacAdminService.importData();
    }

    @DeleteMapping(value = "/delete-data")
    @ResponseStatus(HttpStatus.OK)
    // @Secured("admin")
    public void deleteData(Principal principal) {
        pacAdminService.deleteAllData();
    }

}
