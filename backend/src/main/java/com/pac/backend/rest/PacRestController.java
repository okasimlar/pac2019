package com.pac.backend.rest;

import com.pac.backend.domain.*;
import com.pac.backend.dto.ConferenceDetails;
import com.pac.backend.dto.RoomDetail;
import com.pac.backend.dto.TalkDetails;
import com.pac.backend.service.ConferenceService;
import com.pac.backend.service.PacAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rs")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PacRestController {

    @Autowired
    ConferenceService conferenceService;

    @Autowired
    PacAdminService pacAdminService;

    @GetMapping(path = "/conferences")
    @ResponseStatus(HttpStatus.OK)
    public List<Conference> getAllConferences() {
        return conferenceService.getAllConferences();
    }

    @GetMapping(path = "/locations")
    public List<Location> getAllLocations() {
        return conferenceService.getAllLocations();
    }

    @GetMapping(path = "/location-details/{locationId}")
    public Location getAllLocations(@PathVariable( "locationId" ) Long id) {
        return conferenceService.getLocationById(id);
    }

    @GetMapping(path = "/talks")
    public List<Talk> getTalksForConference(@RequestParam("conferenceId") Long conferenceId) {
        return new ArrayList<>();
    }

    @GetMapping(path = "/conference-details/{conferenceId}")
    public ConferenceDetails getDetailsForConference(@PathVariable("conferenceId") Long conferenceId) {
        return conferenceService.getDetailsForConference(conferenceId);
    }

    @GetMapping(path = "/talk-details/{talkId}")
    public TalkDetails getTalkDetails(@PathVariable("talkId") Long talkId) {
        return conferenceService.getTalkDetails(talkId);
    }

    @GetMapping(path = "/room-details/{roomId}")
    public RoomDetail getRoomDetails(@PathVariable("roomId") Long roomId, @RequestParam("conferenceId") Long conferenceId) {
        return conferenceService.getRoomDetails(roomId, conferenceId);
    }

    @GetMapping(path = "/speakers")
    public List<Speaker> getSpeakers(@RequestParam("conferenceId") Long conferenceId) {
        return conferenceService.getSpeakersByConferenceId(conferenceId);
    }

    @GetMapping(path = "/timeslot-details/{timeslotId}")
    public Timeslot getTimeSlotDetails(@PathVariable("timeslotId") Long timeslotId) {
        return conferenceService.getTimeSlotDetails(timeslotId);
    }

    @GetMapping(path = "/topic-details/{topicId}")
    public List<Talk> getTopicTalks(@PathVariable("topicId") Long timeslotId, @RequestParam("conferenceId") Long conferenceId) {
        return conferenceService.getTopicDetails(timeslotId, conferenceId);
    }

    @GetMapping(path = "/search-locations")
    public List<Location> getLocations(@RequestParam("searchText") String searchText) {
        return conferenceService.searchLocations(searchText);
    }

    @DeleteMapping(path = "/delete-location/{locationId}")
    public void deleteLocation(@PathVariable("locationId") long locationId) {
        conferenceService.deleteLocation(locationId);
    }

    @GetMapping(path = "/search-topics")
    public List<Topic> getTopics(@RequestParam("searchText") String searchText) {
        return conferenceService.searchTopics(searchText);
    }

    @DeleteMapping(path = "/delete-topic/{topicId}")
    public void deleteTopic(@PathVariable("topicId") long topicId) {
        conferenceService.deleteTopic(topicId);
    }

    @GetMapping(path = "/search-speakers")
    public List<Speaker> getSpeakers(@RequestParam("searchText") String searchText) {
        return conferenceService.searchSpeakers(searchText);
    }

    @DeleteMapping(path = "/delete-location/{speakerId}")
    public void deleteSpeaker(@PathVariable("speakerId") long speakerId) {
        conferenceService.deleteSpeaker(speakerId);
    }

    @PostMapping(path = "/add-topic")
    public void addTopic(@RequestBody Topic topic) {
        conferenceService.addTopic(topic);
    }
}
