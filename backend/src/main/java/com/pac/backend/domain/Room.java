package com.pac.backend.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
public class Room {

    @Id
    @GeneratedValue
    @Getter
    @CsvBindByName
    private Long id;

    @Setter
    @Getter
    @CsvBindByName
    private String name;

    @Setter
    @Getter
    @CsvBindByName
    private Integer capacity;

    @Setter
    @Getter
    @CsvBindByName
    private Integer floor;

}
