package com.pac.backend.domain;

import com.opencsv.bean.CsvBindByPosition;
import lombok.*;

@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
public class NodeRelation {

    @Getter
    @Setter
    @CsvBindByPosition(position = 0)
    private Integer fromNodeId;

    @Getter
    @Setter
    @CsvBindByPosition(position = 1)
    private Integer toNodeId;

}
