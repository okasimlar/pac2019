package com.pac.backend.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
public class Conference {

    @Id
    @GeneratedValue
    @CsvBindByName
    private Long id;

    @CsvBindByName
    private String name;

    @Getter
    @Setter
    @CsvBindByName
    private String description;

    @Getter
    @Setter
    @CsvBindByName
    private String from;

    @Getter
    @Setter
    @CsvBindByName
    private String to;

    @Relationship(type="TAKES_PLACE", direction = Relationship.OUTGOING)
    private Location location;


    public void takesPlace(Location location) {
        this.location = location;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Long getId() {
        return id;
    }
}
