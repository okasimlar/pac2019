package com.pac.backend.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
public class Location {

    @Id
    @GeneratedValue
    @Getter
    @CsvBindByName
    private Long id;

    @Getter
    @Setter
    @CsvBindByName
    private String name;

    @Getter
    @Setter
    @CsvBindByName
    private String city;

    @Getter
    @Setter
    @CsvBindByName
    private String address;

    @Getter
    @Setter
    @CsvBindByName
    private String description;

}
