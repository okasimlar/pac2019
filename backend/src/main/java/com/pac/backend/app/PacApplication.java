package com.pac.backend.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@SpringBootApplication(scanBasePackages={"com.pac.backend.*"})
@EnableNeo4jRepositories(basePackages = "com.pac.backend.query")
@EntityScan(basePackages = "com.pac.backend.domain")
@Slf4j
@PropertySources({
		@PropertySource("classpath:application-${envTarget:dev}.properties")
})
public class PacApplication {

	public static void main(String[] args) {
		log.info("Starting Pac Backend Application....");
		SpringApplication.run(PacApplication.class, args);
		log.info("Started Pac Backend Application");
	}

}
