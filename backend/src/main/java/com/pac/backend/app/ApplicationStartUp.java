package com.pac.backend.app;

import com.pac.backend.service.PacAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
class ApplicationStartUp implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    PacAdminService pacAdminService;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        log.info("Starting Spring boot application");
        pacAdminService.importData();
    }

}
