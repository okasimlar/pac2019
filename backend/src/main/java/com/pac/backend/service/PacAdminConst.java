package com.pac.backend.service;

class PacAdminConst {

    static final String LOCATIONS_CSV = "locations.csv";
    static final String CONFERENCES_CSV = "conferences.csv";
    static final String ROOMS_CSV = "rooms.csv";
    static final String SPEAKERS_CSV = "speakers.csv";
    static final String TALKS_CSV = "talks.csv";
    static final String TIMESLOTS_CSV = "timeslots.csv";
    static final String TOPICS_CSV = "topics.csv";
    static final String CONFERENCE_LOCATION_CSV = "conference_location.csv";
    static final String ROOM_LOCATION_CSV = "room_location.csv";
    static final String SPEAKER_TALK_CSV = "speaker_talk.csv";
    static final String TALK_CONFERENCE_CSV = "talk_conference.csv";
    static final String TALK_TOPIC_CSV = "talk_topic.csv";
    static final String TALK_TIMESLOT_CSV = "talk_timeslot.csv";
    static final String SPEAKER_TIMESLOT_CSV = "speaker_timeslot.csv";
    static final String TIMESLOT_ROOM_CSV = "timeslot_room.csv";
    static final String TOPIC_TOPIC_CSV = "topic_topic.csv";
    static final String ID = "id";
    static final String NAME = "name";
    static final String CITY = "city";
    static final String ADDRESS = "address";
    static final String DESCRIPTION = "description";
    static final String CAPACITY = "capacity";
    static final String FLOOR = "floor";
    static final String PROFESSION = "profession";
    static final String BIRTH_DATE = "birthDate";
    static final String FROM = "from";
    static final String TO = "to";
    static final String CONF_ID = "confId";
    static final String LOCATION_ID = "locationId";
    static final String ROOM_ID = "roomId";
    static final String SPEAKER_ID = "speakerId";
    static final String TALK_ID = "talkId";
    static final String TOPIC_ID = "topicId";
    static final String TIMESLOT_ID = "timeslotId";
    static final String FATHER_TOPIC_ID = "fatherTopicId";

}
