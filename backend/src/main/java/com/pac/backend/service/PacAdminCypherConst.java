package com.pac.backend.service;

public class PacAdminCypherConst {

    static final String CREATE_LOCATION_CYPHER = "CREATE (a:Location) " +
            "SET a.id = $id, a.name = $name, a.city = $city, a.address = $address, a.description = $description";

    static final String CREATE_CONFERENCE_CYPHER = "CREATE (a:Conference) " +
            "SET a.id = $id, a.name = $name, a.description = $description, a.from = $from, a.to = $to";

    static final String CREATE_ROOM_CYPHER = "CREATE (a:Room) " +
            "SET a.id = $id, a.name = $name, a.capacity = $capacity, a.floor = $floor";

    static final String CREATE_SPEAKER_CYPHER = "CREATE (a:Speaker) " +
            "SET a.id = $id, a.name = $name, a.birthDate = $birthDate, a.profession= $profession";

    static final String CREATE_TALK_CYPHER = "CREATE (a:Talk) " +
            "SET a.id = $id, a.name = $name, a.description = $description";

    static final String CREATE_TIMESLOT_CYPHER = "CREATE (a:Timeslot) " +
            "SET a.id = $id, a.from = $from, a.to = $to ";

    static final String CREATE_TOPICS_CYPHER = "CREATE (a:Topic) " +
            "SET a.id = $id, a.name = $name";

    static final String RELATION_CONF_LOCATION_CYPHER = "MATCH (c:Conference), (l:Location) " +
        "WHERE c.id = $confId AND l.id = $locationId " +
        "CREATE (c)-[r:TAKES_PLACE]->(l)";

    static final String RELATION_ROOM_LOCATION_CYPHER = "MATCH (a:Room), (l:Location) " +
            "WHERE a.id = $roomId AND l.id = $locationId " +
            "CREATE (a)-[r:IS_IN]->(l)";

    static final String RELATION_SPEAKER_TALK_CYPHER = "MATCH (s:Speaker), (t:Talk) " +
            "WHERE s.id = $speakerId AND t.id = $talkId " +
            "CREATE (s)-[r:TALKS]->(t)";

    static final String RELATION_TALK_CONFERENCE_CYPHER = "MATCH (t:Talk), (c:Conference) " +
            "WHERE t.id = $talkId AND c.id = $confId " +
            "CREATE (t)-[r:IS_HOLD]->(c)";

    static final String RELATION_TALK_TOPIC_CYPHER = "MATCH (t:Talk), (c:Topic) " +
            "WHERE t.id = $talkId AND c.id = $topicId " +
            "CREATE (t)-[r:HAS]->(c)";

    static final String RELATION_TALK_TIMESLOT_CYPHER = "MATCH (t:Talk), (s:Timeslot) " +
            "WHERE t.id = $talkId AND s.id = $timeslotId " +
            "CREATE (t)-[r:IS_IN]->(s)";

    static final String RELATION_SPEAKER_TIMESLOT_CYPHER = "MATCH (s:Speaker), (t:Timeslot) " +
            "WHERE s.id = $speakerId AND t.id = $timeslotId " +
            "CREATE (s)-[r:HAS]->(t)";

    static final String RELATION_TIMESLOT_ROOM_CYPHER = "MATCH (t:Timeslot), (a:Room) " +
            "WHERE t.id = $timeslotId AND a.id = $roomId " +
            "CREATE (t)-[r:IS_IN]->(a)";

    static final String RELATION_TOPIC_TOPIC = "MATCH (t:Topic), (s:Topic) " +
            "WHERE t.id = $fatherTopicId AND s.id = $topicId " +
            "CREATE (t)-[r:IS_FATHER]->(s)";
}
