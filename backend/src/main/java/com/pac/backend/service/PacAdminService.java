package com.pac.backend.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.pac.backend.domain.*;
import com.pac.backend.query.ConferenceRepository;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.neo4j.driver.v1.Values.parameters;
import static com.pac.backend.service.PacAdminConst.*;

@Service
public class PacAdminService {

    private static final String CLASSPATH_NODES = "/app/data/";
    private static final String CLASSPATH_RELATIONS = CLASSPATH_NODES + "relations/";

    @Autowired
    Driver driver;

    @Autowired
    private ConferenceRepository conferenceRepository;

    @Transactional
    public void deleteAllData() {
        String deleteAllCypher = "MATCH (n) DETACH DELETE n";
        executeCypher(deleteAllCypher);
    }

    @Transactional
    public void importData() {

        if (dataAlreadyImported()) {
            return;
        }

        // node-items
        List<Location> locations = parseCSVFile(CLASSPATH_NODES + LOCATIONS_CSV, Location.class);
        List<Conference> conferences = parseCSVFile(CLASSPATH_NODES + CONFERENCES_CSV, Conference.class);
        List<Room> rooms = parseCSVFile(CLASSPATH_NODES + ROOMS_CSV, Room.class);
        List<Speaker> speakers= parseCSVFile(CLASSPATH_NODES + SPEAKERS_CSV, Speaker.class);
        List<Talk> talks = parseCSVFile(CLASSPATH_NODES + TALKS_CSV, Talk.class);
        List<Timeslot> timeslots = parseCSVFile(CLASSPATH_NODES + TIMESLOTS_CSV, Timeslot.class);
        List<Topic> topics = parseCSVFile(CLASSPATH_NODES + TOPICS_CSV, Topic.class);

        // relations
        List<NodeRelation> conferenceLocation =
                parseCSVFile(CLASSPATH_RELATIONS + CONFERENCE_LOCATION_CSV, NodeRelation.class, 1);
        List<NodeRelation> roomLocation =
                parseCSVFile(CLASSPATH_RELATIONS + ROOM_LOCATION_CSV, NodeRelation.class, 1);
        List<NodeRelation> speakerTalk =
                parseCSVFile(CLASSPATH_RELATIONS + SPEAKER_TALK_CSV, NodeRelation.class, 1);
        List<NodeRelation> talkConference =
                parseCSVFile(CLASSPATH_RELATIONS + TALK_CONFERENCE_CSV, NodeRelation.class, 1);
        List<NodeRelation> talkTopic =
                parseCSVFile(CLASSPATH_RELATIONS + TALK_TOPIC_CSV, NodeRelation.class, 1);
        List<NodeRelation> talkTimeslot =
                parseCSVFile(CLASSPATH_RELATIONS + TALK_TIMESLOT_CSV, NodeRelation.class, 1);
        List<NodeRelation> speakerTimeslot =
                parseCSVFile(CLASSPATH_RELATIONS + SPEAKER_TIMESLOT_CSV, NodeRelation.class, 1);
        List<NodeRelation> timeslotRoom =
                parseCSVFile(CLASSPATH_RELATIONS + TIMESLOT_ROOM_CSV, NodeRelation.class, 1);
        List<NodeRelation> topicTopic =
                parseCSVFile(CLASSPATH_RELATIONS + TOPIC_TOPIC_CSV, NodeRelation.class, 1);



        locations.forEach( l -> executeCypher(PacAdminCypherConst.CREATE_LOCATION_CYPHER,
               new Object[]{ID, l.getId(), NAME, l.getName(), CITY, l.getCity(), ADDRESS, l.getAddress(),
                       DESCRIPTION, l.getDescription()}));
        conferences.forEach( c -> executeCypher(PacAdminCypherConst.CREATE_CONFERENCE_CYPHER,
                new Object[]{ID, c.getId(), NAME, c.getName(), DESCRIPTION, c.getDescription(), FROM, c.getFrom(), TO, c.getTo()}));
        rooms.forEach( l -> executeCypher(PacAdminCypherConst.CREATE_ROOM_CYPHER,
                new Object[]{ID, l.getId(), NAME, l.getName(), CAPACITY, l.getCapacity(), FLOOR, l.getFloor()}));
        speakers.forEach( s -> executeCypher(PacAdminCypherConst.CREATE_SPEAKER_CYPHER,
                new Object[]{ID, s.getId(), NAME, s.getName(), BIRTH_DATE, s.getBirthDate(), PROFESSION, s.getProfession()}));
        talks.forEach( l -> executeCypher(PacAdminCypherConst.CREATE_TALK_CYPHER,
                new Object[]{ID, l.getId(), NAME, l.getName(), DESCRIPTION, l.getDescription()}));
        timeslots.forEach( l -> executeCypher(PacAdminCypherConst.CREATE_TIMESLOT_CYPHER,
                new Object[]{ID, l.getId(), FROM, l.getFrom(), TO, l.getTo()}));
        topics.forEach( l -> executeCypher(PacAdminCypherConst.CREATE_TOPICS_CYPHER,
                new Object[]{ID, l.getId(), NAME, l.getName()}));


        conferenceLocation.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_CONF_LOCATION_CYPHER,
                new Object[]{CONF_ID, r.getFromNodeId(), LOCATION_ID, r.getToNodeId()}));

        roomLocation.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_ROOM_LOCATION_CYPHER,
                new Object[]{ROOM_ID, r.getFromNodeId(), LOCATION_ID, r.getToNodeId()}));

        speakerTalk.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_SPEAKER_TALK_CYPHER,
                new Object[]{SPEAKER_ID, r.getFromNodeId(), TALK_ID, r.getToNodeId()}));

        talkConference.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_TALK_CONFERENCE_CYPHER,
                new Object[]{TALK_ID, r.getFromNodeId(), CONF_ID, r.getToNodeId()}));

        talkTopic.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_TALK_TOPIC_CYPHER,
                new Object[]{TALK_ID, r.getFromNodeId(), TOPIC_ID, r.getToNodeId()}));

        talkTimeslot.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_TALK_TIMESLOT_CYPHER,
                new Object[]{TALK_ID, r.getFromNodeId(), TIMESLOT_ID, r.getToNodeId()}));

        speakerTimeslot.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_SPEAKER_TIMESLOT_CYPHER,
                new Object[]{SPEAKER_ID, r.getFromNodeId(), TIMESLOT_ID, r.getToNodeId()}));

        timeslotRoom.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_TIMESLOT_ROOM_CYPHER,
                new Object[]{TIMESLOT_ID, r.getFromNodeId(), ROOM_ID, r.getToNodeId()}));

        topicTopic.forEach(r -> executeCypher(PacAdminCypherConst.RELATION_TOPIC_TOPIC,
                new Object[]{FATHER_TOPIC_ID, r.getFromNodeId(), TOPIC_ID, r.getToNodeId()}));
    }


    private boolean dataAlreadyImported() {
        return conferenceRepository.findAll().iterator().hasNext();
    }


    private void executeCypher(String cypher, Object... objects) {
        try (Session session = driver.session()) {
            session.writeTransaction(tx -> {
                tx.run(cypher, parameters(objects));
                return "";
            });
        }
    }

    private <T> List<T> parseCSVFile(String resourceLocation, Class<T> cls) {
        return parseCSVFile(resourceLocation, cls, 0);
    }

    private <T> List<T> parseCSVFile(String resourceLocation, Class<T> cls, int skipFiles) {

        try (Reader reader = new FileReader(new File(resourceLocation))) {
            CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(reader)
                    .withType(cls)
                    .withSkipLines(skipFiles)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            Iterator<T> csvUserIterator = csvToBean.iterator();
            List<T> parsedItems = new ArrayList<>();
            csvUserIterator.forEachRemaining(parsedItems::add);
            return parsedItems;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
