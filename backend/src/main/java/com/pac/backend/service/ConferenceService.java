package com.pac.backend.service;

import com.pac.backend.domain.*;
import com.pac.backend.dto.ConferenceDetails;
import com.pac.backend.dto.RoomDetail;
import com.pac.backend.dto.RoomSchedule;
import com.pac.backend.dto.TalkDetails;
import com.pac.backend.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class ConferenceService {

    private static final String DD_MM_YYYY_HH_MM_SS = "dd.MM.yyyy HH:mm:ss";
    private static final String DD_MM_YYYY = "dd.MM.yyyy";


    @Autowired
    private ConferenceRepository conferenceRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private TalkRepository talkRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private SpeakerRepository speakerRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @Transactional(readOnly = true)
    public List<Conference> getAllConferences() {
        Iterable<Conference> conferenceIterable = conferenceRepository.findAll();
        List<Conference> conferences = new ArrayList<>();
        conferenceIterable.forEach(conferences::add);
        return conferences;
    }

    @Transactional(readOnly = true)
    public List<Location> getAllLocations() {
        Iterable<Location> locationIterable = locationRepository.findAll();
        List<Location> locations = new ArrayList<>();
        locationIterable.forEach(locations::add);
        return locations;
    }

    @Transactional(readOnly = true)
    public Location getLocationById(Long id) {
        Optional<Location> byId = locationRepository.findById(id);
        return byId.orElse(null);
    }

    @Transactional(readOnly = true)
    public List<Talk> getTalksForConference(Long conferenceId) {
        return new ArrayList<>();
    }

    @Transactional(readOnly = true)
    public ConferenceDetails getDetailsForConference(Long conferenceId) {
        List<Talk> talks = talkRepository.findByConferenceId(conferenceId);
        List<Room> rooms = roomRepository.findByConferenceId(conferenceId);
        List<Speaker> speakers = speakerRepository.findSpeakersByConferenceId(conferenceId);
        List<Topic> topics = topicRepository.findTopicByConferenceId(conferenceId);
        Conference conference = conferenceRepository.findById(conferenceId).orElse(null);
        return ConferenceDetails.builder().conference(conference).rooms(rooms)
                .talks(talks).speakers(speakers).topics(topics).build();
    }

    @Transactional(readOnly = true)
    public TalkDetails getTalkDetails(Long talkId) {
        Optional<Talk> talk = talkRepository.findById(talkId);
        List<Topic> topics = topicRepository.findTopicByConferenceId(talkId);
        return TalkDetails.builder().talk(talk.orElse(null)).topics(topics).build();
    }

    @Transactional(readOnly = true)
    public RoomDetail getRoomDetails(Long roomId, Long conferenceId) {
        List<Timeslot> timeslots = timeSlotRepository.findByRoomIdAndConferenceId(roomId, conferenceId);
        RoomDetail roomDetail = getRoomScheduleDTO(timeslots);
        if (timeslots.size() > 0) {
            roomDetail.setRoom(timeslots.get(0).getRoom());
        }
        return roomDetail;
    }

    @Transactional(readOnly = true)
    public List<Talk> getTopicDetails(Long topicId, Long conferenceId) {
        return talkRepository.findByTopicIdAndConferenceId(topicId, conferenceId);
    }

    @Transactional(readOnly = true)
    public List<Speaker> getSpeakersByConferenceId(Long conferenceId) {
        return speakerRepository.findSpeakersByConferenceId( conferenceId);
    }

    @Transactional(readOnly = true)
    public Timeslot getTimeSlotDetails(Long timeslotId) {
        return timeSlotRepository.findById(timeslotId).orElse(null);
    }


    @Transactional(readOnly = true)
    public List<Location> searchLocations(String searchText) {
        return locationRepository.findByName(searchText);
    }

    @Transactional(readOnly = true)
    public List<Topic> searchTopics(String searchText) {
        return topicRepository.findByName(searchText);
    }

    @Transactional(readOnly = true)
    public List<Speaker> searchSpeakers(String searchText) {
        return speakerRepository.findByName(searchText);
    }

    private RoomDetail getRoomScheduleDTO(List<Timeslot> timeslots) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(DD_MM_YYYY_HH_MM_SS);
        DateTimeFormatter dayFormatter = DateTimeFormatter.ofPattern(DD_MM_YYYY);
        timeslots.sort(Comparator.comparing(s -> LocalDateTime.parse(s.getFrom(), timeFormatter)));
        Map<String, List<Timeslot>> dayTalkMap = new HashMap<>();
        timeslots.forEach(t ->
                {
                    LocalDateTime dateTime = LocalDateTime.parse(t.getFrom(), timeFormatter);
                    String dayOfTalk = dateTime.toLocalDate().format(dayFormatter);
                    if (dayTalkMap.containsKey(dayOfTalk)) {
                        dayTalkMap.get(dayOfTalk).add(t);
                    } else {
                        List<Timeslot> slots = new ArrayList<>();
                        slots.add(t);
                        dayTalkMap.put(dayOfTalk, slots);
                    }
                }
        );
        RoomDetail roomSchedules = RoomDetail.builder().roomSchedules(new ArrayList<>()).build();
        dayTalkMap.forEach((key, value) -> roomSchedules.getRoomSchedules().add((RoomSchedule.builder().date(key).timeslots(value).build())));
        roomSchedules.getRoomSchedules().sort(Comparator.comparing(s -> LocalDateTime.parse(s.getDate() + " 05:00:00", timeFormatter)));
        return roomSchedules;
    }

    @Transactional(readOnly = true)
    public void deleteLocation(long locationId) {
        locationRepository.deleteById(locationId);
    }

    @Transactional(readOnly = true)
    public void deleteSpeaker(long speakerId) {
        speakerRepository.deleteById(speakerId);
    }

    @Transactional(readOnly = true)
    public void deleteTopic(long topicId) {
        topicRepository.deleteById(topicId);
    }

    @Transactional(readOnly = true)
    public void addTopic(Topic topic) {
        topicRepository.save(topic);
    }
}
