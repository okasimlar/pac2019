package com.pac.init.configuration;

import org.neo4j.driver.v1.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Neo4JConfiguration {

    @Value("${neo4j.scheme}")
    private String scheme;

    @Value("${neo4j.host}")
    private String host;

    @Value("${neo4j.port}")
    private String port;

    @Value("${neo4j.policy:#{null}}")
    private String routingPolicy;

    @Value("${neo4j.auth.type:basic}")
    private String authType;

    @Value("${neo4j.auth.username:#{null}}")
    private String username;

    @Value("${neo4j.auth.password:#{null}}")
    private String password;

    @Value("${neo4j.auth.ticket:#{null}}")
    private String ticket;

    /**
     * Create a new Driver instance
     *
     * @return Driver
     */
    @Bean
    public Driver neo4jDriver() {
        String uri = getUri();
        AuthToken token = getAuthToken();
        return GraphDatabase.driver(uri, token);
    }

    /**
     * Get the URI for the Neo4j Server
     * @return String
     */
    public String getUri() {
        String uri = String.format("%s://%s:%s", scheme, host, port);

        if ( scheme.equals("bolt+routing") && routingPolicy != null ) {
            uri += "?policy="+ routingPolicy;
        }

        return uri;
    }

    /**
     *
     * @return AuthToken
     */
    public AuthToken getAuthToken() {
        return AuthTokens.none();
    }
}
