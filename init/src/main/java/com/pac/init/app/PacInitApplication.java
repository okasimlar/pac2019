package com.pac.init.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@SpringBootApplication(scanBasePackages={"com.pac.init.*", "com.pac.init.query"})
@EnableNeo4jRepositories(basePackages = "com.pac.init.query")
@EntityScan(basePackages = "com.pac.init.domain")
@PropertySources({
		@PropertySource("classpath:application.properties")
})
public class PacInitApplication {

	private final static Logger log = LoggerFactory.getLogger(PacInitApplication.class);

	public static void main(String[] args) {
		log.info("Starting Spring-Boot application.");
		SpringApplication.run(PacInitApplication.class, args);
		System.exit(0);
	}
}
