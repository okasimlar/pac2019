package com.pac.init.app;

import com.pac.init.service.PacAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    PacAdminService pacAdminService;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        pacAdminService.importData();
    }

}
