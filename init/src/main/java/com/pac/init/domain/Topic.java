package com.pac.init.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
public class Topic {

    @Id
    @GeneratedValue
    @Getter
    @CsvBindByName
    private Long id;

    @Getter
    @Setter
    @CsvBindByName
    private String name;

}
