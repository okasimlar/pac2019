package com.pac.init.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
public class Timeslot {

    @Id
    @GeneratedValue
    @Getter
    @CsvBindByName
    private Long id;

    @Getter
    @Setter
    @CsvBindByName
    private String from;

    @Getter
    @Setter
    @CsvBindByName
    private String to;

    @Getter
    @Setter
    @Relationship(type = "IS_IN", direction = Relationship.OUTGOING)
    private Room room;


    @Getter
    @Setter
    @Relationship(type = "IS_IN", direction = Relationship.INCOMING)
    private Talk talk;

    @Getter
    @Setter
    @Relationship(type = "HAS", direction = Relationship.INCOMING)
    private Speaker speaker;

}

