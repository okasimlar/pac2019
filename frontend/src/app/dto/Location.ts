export class Location {
  public name: string;
  public description: string;
  public address: string;
  public city: string;

  constructor() {

  }
}
