import { Component, OnInit } from '@angular/core';
import { RestClientService } from '../rest-client.service';
import { Router } from '@angular/router';
import { Location } from '../dto/Location';
import { Topic } from '../dto/Topic';
import { Speaker } from '../dto/Speaker';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  locations: object;
  topics: object;
  speakers: object;
  newLocation = new Location();
  newTopic = new Topic();
  newSpeaker = new Speaker();

  constructor(private backendService: RestClientService, private router: Router) { }

  ngOnInit() {
  }

  importData() {
    this.backendService.importData();
  }

  deleteData() {
    this.backendService.deleteData();
  }

  searchLocations(searchText) {
    this.backendService.searchLocations(searchText).subscribe(data => {
        this.locations = data;
      }
    );
  }

  searchTopics(searchText) {
    this.backendService.searchTopics(searchText).subscribe(data => {
        this.topics = data;
      }
    );
  }

  searchSpeakers(searchText) {
    this.backendService.searchSpeakers(searchText).subscribe(data => {
        this.speakers = data;
      }
    );
  }

  deleteLocation(location) {
    this.backendService.deleteLocation(location.id).subscribe(data => {});
    this.removeByAttr(this.locations, 'id', location.id);
  }

  deleteTopic(topic) {
    this.backendService.deleteTopic(topic.id).subscribe(data => {});
    this.removeByAttr(this.topics, 'id', topic.id);
  }

  deleteSpeaker(speaker) {
    this.backendService.deleteSpeaker(speaker.id).subscribe(data => {});
    this.removeByAttr(this.speakers, 'id', speaker.id);
  }

  addNewLocation() {
    alert(this.newLocation.name);
  }

  addNewTopic() {
    this.backendService.addTopic(this.newTopic).subscribe(data => {});
  }

  addNewSpeaker() {
    this.backendService.addSpeaker(this.newSpeaker).subscribe(data => {});
  }

  removeByAttr(arr, attr, value) {
    let i = arr.length;
    while (i--) {
      if ( arr[i]
        && arr[i].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[i][attr] === value ) ) {
        arr.splice (i, 1);
      }
    }
    return arr;
  }
}
