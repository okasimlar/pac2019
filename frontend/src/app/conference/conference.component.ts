import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-conference',
  templateUrl: './conference.component.html',
  styleUrls: ['./conference.component.scss']
})
export class ConferenceComponent implements OnInit {

  conferences: object;

  constructor(private backendService: RestClientService, private router: Router) { }

  ngOnInit() {
    this.backendService.getConferences().subscribe(data => {
        this.conferences = data;
        console.log(this.conferences);
      }
    );
  }

}
