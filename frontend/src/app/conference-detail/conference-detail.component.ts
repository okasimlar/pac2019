import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {RestClientService} from '../rest-client.service';

@Component({
  selector: 'app-conference-detail',
  templateUrl: './conference-detail.component.html',
  styleUrls: ['./conference-detail.component.scss']
})
export class ConferenceDetailComponent implements OnInit {

  conferenceDetails: object;


  constructor(private backendService: RestClientService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const conferenceId = params.get('id');
      this.backendService.getConferenceDetails(conferenceId).subscribe(data => {
        this.conferenceDetails = data;
      });
    });
  }
}
