import { KeycloakService } from 'keycloak-angular';
import {environment} from '../../environments/environment';

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  // return (): Promise<any> => keycloak.init();
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: {
            url: 'http://localhost:8180/auth/',
            //url: 'http://keycloak.example.com/auth/',
            realm: 'SpringBootKeycloak',
            clientId: 'pac-app',
          },
          initOptions: {
            onLoad: 'check-sso',
            checkLoginIframe: false
          },
          enableBearerInterceptor: true,
          bearerExcludedUrls: ['/confereences']
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };

}
