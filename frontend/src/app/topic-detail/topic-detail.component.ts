import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-topic-detail',
  templateUrl: './topic-detail.component.html',
  styleUrls: ['./topic-detail.component.scss']
})
export class TopicDetailComponent implements OnInit {

  talks: object;

  constructor(private backendService: RestClientService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.route.queryParamMap.subscribe(queryParams => {
        const conferenceId = queryParams.get('conferenceId');
        const topicId = params.get('topicId');
        this.backendService.getTopicDetails(topicId, conferenceId).subscribe(data => {
          this.talks = data;
        });
      });
    });
  }
}
