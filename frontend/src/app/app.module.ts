import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';
import { initializer } from './utils/app-init';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ConferenceComponent } from './conference/conference.component';
import { LocationComponent } from './location/location.component';
import { HttpClientModule } from '@angular/common/http';
import { ConferenceDetailComponent } from './conference-detail/conference-detail.component';
import { LocationDetailComponent } from './location-detail/location-detail.component';
import { TalkDetailComponent } from './talk-detail/talk-detail.component';
import { RoomDetailComponent } from './room-detail/room-detail.component';
import { TimeslotDetailComponent } from './timeslot-detail/timeslot-detail.component';
import { AdminComponent } from './admin/admin.component';
import { TopicDetailComponent } from './topic-detail/topic-detail.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ConferenceComponent,
    LocationComponent,
    ConferenceDetailComponent,
    LocationDetailComponent,
    TalkDetailComponent,
    RoomDetailComponent,
    TimeslotDetailComponent,
    AdminComponent,
    TopicDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    KeycloakAngularModule,
    AngularFontAwesomeModule,
    FormsModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
