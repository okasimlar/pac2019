import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.scss']
})
export class RoomDetailComponent implements OnInit {

  roomDetail: object;

  constructor(private backendService: RestClientService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.route.queryParamMap.subscribe(queryParams => {
        const conferenceId = queryParams.get('conferenceId');
        const roomId = params.get('roomId');
        this.backendService.getRoomDetails(roomId, conferenceId).subscribe(data => {
          this.roomDetail = data;
        });
      });
    });
  }

}
