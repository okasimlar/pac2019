import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-timeslot-detail',
  templateUrl: './timeslot-detail.component.html',
  styleUrls: ['./timeslot-detail.component.scss']
})
export class TimeslotDetailComponent implements OnInit {

  timeslot: object;

  constructor(private backendService: RestClientService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const timeslotId = params.get('timeslotId');
      this.backendService.getTimeSlotDetails(timeslotId).subscribe(data => {
        this.timeslot = data;
      });
    });
  }

}
