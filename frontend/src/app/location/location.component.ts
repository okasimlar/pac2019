import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  locations: object;

  constructor(private backendService: RestClientService) { }

  ngOnInit() {
    this.backendService.getLocations().subscribe(data => {
        this.locations = data;
        console.log(this.locations);
      }
    );
  }

}
