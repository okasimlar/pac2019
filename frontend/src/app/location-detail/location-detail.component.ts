import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.scss']
})
export class LocationDetailComponent implements OnInit {

  locationDetail: object;

  constructor(private backendService: RestClientService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const locationId = params.get('id');
      this.backendService.getLocationDetails(locationId).subscribe(data => {
        this.locationDetail = data;
      });
    });
  }

}
