import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  title = 'Conference System';

  constructor(private backendService: RestClientService) { }

  ngOnInit() {
  }
}
