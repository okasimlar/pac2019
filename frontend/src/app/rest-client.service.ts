import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../environments/environment';
import {Speaker} from './dto/Speaker';
import {Topic} from './dto/Topic';

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  readonly conferenceBackendUrl = environment.baseUrl + '/rs';
  readonly conferenceAdminBackendUrl = environment.baseUrl + '/admin';

  constructor(private http: HttpClient) { }

  getConferences() {
    const conferencesPath = '/conferences';
    return this.http.get(this.conferenceBackendUrl + conferencesPath);
  }

  getConferenceDetails(conferenceId) {
    const conferenceDetailPath = '/conference-details';
    return this.http.get(this.conferenceBackendUrl + conferenceDetailPath + '/' + conferenceId);
  }

  getLocationDetails(locationId) {
    const conferenceDetailPath = '/location-details';
    return this.http.get(this.conferenceBackendUrl + conferenceDetailPath + '/' + locationId);
  }

  getTalkDetails(talkId) {
    const conferenceDetailPath = '/talk-details';
    return this.http.get(this.conferenceBackendUrl + conferenceDetailPath + '/' + talkId);
  }

  getLocations() {
    const locationsPath = '/locations';
    return this.http.get(this.conferenceBackendUrl + locationsPath);
  }

  getRoomDetails(roomId, conferenceId) {
    const roomDetailsPath = '/room-details';
    return this.http.get(this.conferenceBackendUrl + roomDetailsPath  + '/' + roomId + '?conferenceId=' + conferenceId);
  }

  getTimeSlotDetails(timeslotId) {
    const timeslotDetailsPath = '/timeslot-details';
    return this.http.get(this.conferenceBackendUrl + timeslotDetailsPath  + '/' + timeslotId);
  }

  getTopicDetails(topicId, conferenceId) {
    const topicDetails = '/topic-details';
    return this.http.get(this.conferenceBackendUrl + topicDetails  + '/' + topicId + '?conferenceId=' + conferenceId);
  }

  importData() {
    const importPath = '/import-data';
    this.http.put(this.conferenceAdminBackendUrl + importPath, {})
      .subscribe(data  => {
          console.log('PUT Request is successful', data);
        },
        error  => {

          console.log('Error', error);

        }

      );
  }

  deleteData() {
    const deletePath = '/delete-data';
    this.http.delete(this.conferenceAdminBackendUrl + deletePath, {})
      .subscribe(data => {
          console.log('Delete Request is successful', data);
        },
        error => {
          console.log('Error', error);
        }

      );
  }

  searchLocations(searchText) {
    const searchLocationsPath = '/search-locations';
    return this.http.get(this.conferenceBackendUrl + searchLocationsPath +  '?searchText=' + searchText, {});
  }

  searchTopics(searchText) {
    const searchLocationsPath = '/search-topics';
    return this.http.get(this.conferenceBackendUrl + searchLocationsPath +  '?searchText=' + searchText, {});
  }

  searchSpeakers(searchText) {
    const searchLocationsPath = '/search-speakers';
    return this.http.get(this.conferenceBackendUrl + searchLocationsPath +  '?searchText=' + searchText, {});
  }

  addTopic(topic) {
    const addTopicPath = '/add-topic';
    return this.http.post(this.conferenceBackendUrl + addTopicPath,  topic);
  }

  addSpeaker(speaker) {
    const addSpeakerPath = '/add-speakers';
    return this.http.post(this.conferenceBackendUrl + addSpeakerPath, speaker);
  }

  addLocation(location) {
    const addLocationPath = '/add-location';
    return this.http.post(this.conferenceBackendUrl + addLocationPath, location);
  }

  deleteLocation(locationId) {
    const deleteLocationsPath = '/delete-location';
    return this.http.delete(this.conferenceBackendUrl + deleteLocationsPath  + '/' + locationId, {});
  }

  deleteSpeaker(speakerId) {
    const deleteSpeakerPath = '/delete-speaker';
    return this.http.delete(this.conferenceBackendUrl + deleteSpeakerPath  + '/' + speakerId, {});
  }

  deleteTopic(topicId) {
    const deleteSpeakerPath = '/delete-topic';
    return this.http.delete(this.conferenceBackendUrl + deleteSpeakerPath  + '/' + topicId, {});
  }
}
