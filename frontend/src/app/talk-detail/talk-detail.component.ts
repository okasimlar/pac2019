import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-talk-detail',
  templateUrl: './talk-detail.component.html',
  styleUrls: ['./talk-detail.component.scss']
})
export class TalkDetailComponent implements OnInit {

  talkDetail: object;

  constructor(private backendService: RestClientService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const locationId = params.get('talkid');
      this.backendService.getTalkDetails(locationId).subscribe(data => {
        this.talkDetail = data;
      });
    });
  }

}
