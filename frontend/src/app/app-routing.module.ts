import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConferenceComponent } from './conference/conference.component';
import { LocationComponent } from './location/location.component';
import { ConferenceDetailComponent } from './conference-detail/conference-detail.component';
import { AppAuthGuard } from './app.authguard';
import { LocationDetailComponent } from './location-detail/location-detail.component';
import { TalkDetailComponent } from './talk-detail/talk-detail.component';
import {RoomDetailComponent} from './room-detail/room-detail.component';
import {TimeslotDetailComponent} from './timeslot-detail/timeslot-detail.component';
import {AdminComponent} from './admin/admin.component';
import {TopicDetailComponent} from './topic-detail/topic-detail.component';


const routes: Routes = [
  {
    path: 'conferences',
    component: ConferenceComponent,
  },
  {
    path: 'locations',
    component: LocationComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'conference-detail/:id',
    component: ConferenceDetailComponent
  },
  {
    path: 'location-detail/:id',
    component: LocationDetailComponent
  },
  {
    path: 'talk-detail/:talkid',
    component: TalkDetailComponent
  },
  {
    path: 'room-detail/:roomId',
    component: RoomDetailComponent
  },
  {
    path: 'timeslot-detail/:timeslotId',
    component: TimeslotDetailComponent
  },
  {
    path: 'topic-detail/:topicId',
    component: TopicDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class AppRoutingModule { }
